// Code generated by go-swagger; DO NOT EDIT.

package account

// This file was generated by the swagger tool.
// Editing this file might prove futile when you re-run the swagger generate command

import (
	"io"
	"net/http"

	"github.com/go-openapi/errors"
	"github.com/go-openapi/runtime"
	"github.com/go-openapi/runtime/middleware"

	models "gitlab.com/rtdevops/wallet-core/gen/models"
)

// NewCreateAccountParams creates a new CreateAccountParams object
// no default values defined in spec.
func NewCreateAccountParams() CreateAccountParams {

	return CreateAccountParams{}
}

// CreateAccountParams contains all the bound params for the create account operation
// typically these are obtained from a http.Request
//
// swagger:parameters createAccount
type CreateAccountParams struct {

	// HTTP Request Object
	HTTPRequest *http.Request `json:"-"`

	/*Account currency
	  Required: true
	  In: body
	*/
	Account *models.Account
}

// BindRequest both binds and validates a request, it assumes that complex things implement a Validatable(strfmt.Registry) error interface
// for simple values it will use straight method calls.
//
// To ensure default values, the struct must have been initialized with NewCreateAccountParams() beforehand.
func (o *CreateAccountParams) BindRequest(r *http.Request, route *middleware.MatchedRoute) error {
	var res []error

	o.HTTPRequest = r

	if runtime.HasBody(r) {
		defer r.Body.Close()
		var body models.Account
		if err := route.Consumer.Consume(r.Body, &body); err != nil {
			if err == io.EOF {
				res = append(res, errors.Required("account", "body"))
			} else {
				res = append(res, errors.NewParseError("account", "body", "", err))
			}
		} else {
			// validate body object
			if err := body.Validate(route.Formats); err != nil {
				res = append(res, err)
			}

			if len(res) == 0 {
				o.Account = &body
			}
		}
	} else {
		res = append(res, errors.Required("account", "body"))
	}
	if len(res) > 0 {
		return errors.CompositeValidationError(res...)
	}
	return nil
}
