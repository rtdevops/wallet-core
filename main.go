package main

import (
	"database/sql"
	"fmt"
	"io/ioutil"
	"net/http"
	"os"
	"path/filepath"
	"time"

	log "github.com/go-pkgz/lgr"
	"github.com/gorilla/handlers"
	flag "github.com/jessevdk/go-flags"
	"github.com/justinas/alice"
	_ "github.com/lib/pq"
	"github.com/spf13/afero"
	"gitlab.com/rtdevops/wallet-core/gen/restapi"
	"gitlab.com/rtdevops/wallet-core/internal/account"
	"gitlab.com/rtdevops/wallet-core/internal/payment"
	yaml "gopkg.in/yaml.v2"
)

var (
	appFS   = afero.NewOsFs()
	commit  string
	builtAt string
	builtBy string
	builtOn string
	opts    struct {
		ConfPath string `short:"c" long:"config" description:"Path to the configuration file" default:"config.yml" env:"SHA2GEN_CONFIG_PATH"`
		Dbg      bool   `short:"d" long:"debug" description:"Debug mode" env:"SHA2GEN_DEBUG_MODE"`
	}
)

// Config type stores service configuration (config.yml)
type Config struct {
	Service struct {
		Host string `yaml:"host"`
		Port int    `yaml:"port"`
	} `yaml:"service"`
	Database struct {
		Host     string `yaml:"host"`
		Port     int    `yaml:"port"`
		SSL      bool   `yaml:"ssl"`
		Name     string `yaml:"name"`
		User     string `yaml:"user"`
		Password string `yaml:"password"`
	} `yaml:"database"`
}

func main() {
	if _, err := flag.Parse(&opts); err != nil {
		os.Exit(0)
	}
	setupLog(opts.Dbg)

	log.Printf("INFO wallet-core | commit: %s | built at %s by %s on %s\n", commit, builtAt, builtBy, builtOn)
	conf, sslMode := parseConfig(opts.ConfPath), "disable"
	if conf.Database.SSL {
		sslMode = "require"
	}
	connStr := fmt.Sprintf("host=%s port=%d dbname=%s user=%s password='%s' sslmode=%s",
		conf.Database.Host,
		conf.Database.Port,
		conf.Database.Name,
		conf.Database.User,
		conf.Database.Password,
		sslMode)
	db, err := sql.Open("postgres", connStr)
	if err != nil {
		log.Printf("FATAL %s", err)
	}
	if waitForDB(db, time.Duration(120)) {
		log.Printf("INFO Database connection established")
	} else {
		log.Printf("FATAL Database isn't responding")
	}
	defer db.Close()

	a, p := account.Account{}, payment.Payment{}
	a.DB, p.DB = db, db
	// chain http middleware
	commonMiddleware := alice.New(logMiddleware, handlers.CompressHandler)
	// initiate the http handler, with the objects that are implementing the business logic
	h, err := restapi.Handler(restapi.Config{
		AccountAPI:      &a,
		PaymentAPI:      &p,
		Logger:          log.Printf,
		InnerMiddleware: func(h http.Handler) http.Handler { return commonMiddleware.Then(h) },
	})
	if err != nil {
		log.Printf("FATAL %s", err)
	}

	log.Printf("INFO Starting to serve, access server on http://%s:%d", conf.Service.Host, conf.Service.Port)
	if err := http.ListenAndServe(fmt.Sprintf("%s:%d", conf.Service.Host, conf.Service.Port), h); err != nil {
		log.Printf("FATAL %s", err)
	}
}

func parseConfig(path string) *Config {
	conf := &Config{}
	file, err := appFS.Open(filepath.Clean(path))
	if err != nil {
		log.Printf("FATAL %s", err)
	}
	defer file.Close()

	data, err := ioutil.ReadAll(file)
	if err != nil {
		log.Printf("FATAL %s", err)
	}
	if err := yaml.Unmarshal(data, conf); err != nil {
		log.Printf("FATAL %s", err)
	}
	return conf
}

func waitForDB(db *sql.DB, timeout time.Duration) bool {
	for {
		if timeout <= 0 {
			return false
		}

		if err := db.Ping(); err != nil {
			log.Printf("ERROR Database isn't responding, %s", err)
			time.Sleep(1 * time.Second)
			timeout--
		} else {
			return true
		}
	}
}

func setupLog(dbg bool) {
	if dbg {
		log.Setup(log.Debug, log.CallerFile, log.CallerFunc, log.Msec, log.LevelBraces)
		return
	}
	log.Setup(log.Msec, log.LevelBraces)
}

var logMiddleware = func(h http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		t1 := time.Now()
		h.ServeHTTP(w, r)
		method := r.Method
		path := r.URL.Path
		t2 := time.Now()
		log.Printf("INFO [%s] %s %.2fms", method, path, float64(t2.Sub(t1))/float64(time.Millisecond))
	})
}
