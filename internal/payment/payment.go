// Package payment includes types and methods to handle /payments and /payment/* requests.
package payment

import (
	"context"
	"database/sql"

	"github.com/go-openapi/runtime/middleware"
	"github.com/go-openapi/swag"
	log "github.com/go-pkgz/lgr"
	"gitlab.com/rtdevops/wallet-core/gen/models"
	"gitlab.com/rtdevops/wallet-core/gen/restapi/operations/payment"
)

type pDetails struct {
	currencyID         int
	fromAccountBalance float64
	toAccountBalance   float64
}

// Payment type stores DB connection and information about processing payment.
// Includes information received from the user and values produced at processing time.
type Payment struct {
	DB       *sql.DB
	pModel   *models.Payment
	pDetails pDetails
}

type sendPaymentValidator interface {
	validateSendPaymentParams(p *Payment, v ...func(p *Payment) (int, error)) (int, error)
}

type validator struct{}

// SendPayment method is an entrypoint for /payment/make requests.
func (p *Payment) SendPayment(ctx context.Context, params payment.SendPaymentParams) middleware.Responder {
	code, err := sendPaymentHelper(p.DB, &validator{}, params.Payment)
	if err != nil {
		if code >= 400 && code < 500 {
			log.Printf("WARN %s %s", err, printPaymentAttrs(params.Payment))
		} else {
			log.Printf("ERROR %s %s", err, printPaymentAttrs(params.Payment))
		}
		return payment.NewSendPaymentDefault(code).WithPayload(&models.Error{Message: swag.String(err.Error())})
	}
	return payment.NewSendPaymentOK().WithPayload(params.Payment)
}

// GetPayments in an entrypoint for /payments requests.
func (p *Payment) GetPayments(ctx context.Context, params payment.GetPaymentsParams) middleware.Responder {
	payments, code, err := getPaymentsHelper(p.DB)
	if err != nil {
		if code >= 400 && code < 500 {
			log.Printf("WARN %s", err)
		} else {
			log.Printf("ERROR %s", err)
		}
		return payment.NewGetPaymentsDefault(code).WithPayload(&models.Error{Message: swag.String(err.Error())})
	}
	return payment.NewGetPaymentsOK().WithPayload(payments)
}
