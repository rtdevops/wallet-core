// Package payment includes types and methods to handle /payments and /payment/* requests.
package payment

import (
	"database/sql"
	"fmt"

	log "github.com/go-pkgz/lgr"
	"gitlab.com/rtdevops/wallet-core/gen/models"
	"gitlab.com/rtdevops/wallet-core/internal/account"
	"gitlab.com/rtdevops/wallet-core/internal/constants"
)

// getPaymentsHelper function makes all processing for /payments requests.
// Executes SELECT statement, validates and returns output.
func getPaymentsHelper(db *sql.DB) ([]*models.Payment, int, error) {
	var payments []*models.Payment

	stmt := `SELECT p.id, p.n_from_account_id, p.n_to_account_id, p.n_amount, c.vc_name
					 FROM payments   p,
								currencies c
					 WHERE p.n_currency_id = c.n_currency_id`

	rows, err := db.Query(stmt)
	if err != nil {
		log.Printf("ERROR %s", err)
		return nil, 500, fmt.Errorf("Failed to initiate DB query")
	}
	defer rows.Close()

	for rows.Next() {
		var (
			paymentID     sql.NullInt64
			fromAccountID sql.NullInt64
			toAccountID   sql.NullInt64
			amount        sql.NullFloat64
			currencyName  sql.NullString
		)
		err = rows.Scan(&paymentID, &fromAccountID, &toAccountID, &amount, &currencyName)
		if err != nil {
			log.Printf("ERROR %s", err)
			return nil, 500, fmt.Errorf("Failed to parse returned rowset")
		}
		payments = append(payments, &models.Payment{
			ID:          paymentID.Int64,
			FromAccount: &fromAccountID.Int64,
			ToAccount:   &toAccountID.Int64,
			Amount:      &amount.Float64,
			Currency:    currencyName.String,
		})
	}
	err = rows.Err()
	if err != nil {
		log.Printf("ERROR %s", err)
		return nil, 500, fmt.Errorf("Failed to parse row")
	}
	return payments, 200, nil
}

// sendPaymentHelper function makes all processing for /payment/make requests.
// Validates user input, inserts new values to Payments table and updates user's accounts (all in one transaction).
func sendPaymentHelper(db *sql.DB, v sendPaymentValidator, params *models.Payment) (int, error) {
	setPaymentDefaultCurrency(params)
	p := &Payment{DB: db, pModel: params}
	code, err := v.validateSendPaymentParams(p,
		validatePaymentAccounts, validatePaymentCurrency, validatePaymentAmount)
	if err != nil {
		return code, err
	}
	// update Payments and Accounts in the same transaction
	tx, err := db.Begin()
	if err != nil {
		log.Printf("ERROR %s %s", err, printPaymentAttrs(p.pModel))
		return 500, fmt.Errorf("Failed to initiate new DB transaction %s", printPaymentAttrs(p.pModel))
	}
	// insert into Payments
	paymentID, amount, errCtx := sql.NullInt64{}, float64(0), ""
	stmtPayment := `INSERT INTO payments (n_from_account_id, n_to_account_id, n_amount, n_currency_id) VALUES ($1, $2, $3, $4) RETURNING id, n_amount`
	switch err = tx.QueryRow(stmtPayment, *p.pModel.FromAccount, *p.pModel.ToAccount, *p.pModel.Amount, p.pDetails.currencyID).Scan(&paymentID, &amount); err {
	case sql.ErrNoRows:
		errCtx = "Failed to process payment, no payment ID generated"
	case nil:
		p.pModel.ID, p.pModel.Amount = paymentID.Int64, &amount
	default:
		errCtx = "Unexpected error. Failed to process payment"
	}
	if err != nil {
		tx.Rollback() // nolint: gosec
		log.Printf("ERROR %s %s", err, printPaymentAttrs(p.pModel))
		return 500, fmt.Errorf("%s %s", errCtx, printPaymentAttrs(p.pModel))
	}
	// update Sender's account balance
	if _, err = tx.Exec(`UPDATE accounts SET n_balance = n_balance - $1 WHERE id = $2`, *p.pModel.Amount, *p.pModel.FromAccount); err != nil {
		tx.Rollback() // nolint: gosec
		log.Printf("ERROR %s %s", err, printPaymentAttrs(p.pModel))
		return 500, fmt.Errorf("Unexpected error. Failed to charge sender's account %s", printPaymentAttrs(p.pModel))
	}
	// update Receiver's account balance
	if _, err = tx.Exec(`UPDATE accounts SET n_balance = n_balance + $1 WHERE id = $2`, *p.pModel.Amount, *p.pModel.ToAccount); err != nil {
		tx.Rollback() // nolint: gosec
		log.Printf("ERROR %s %s", err, printPaymentAttrs(p.pModel))
		return 500, fmt.Errorf("Unexpected error. Failed to add funds to receiver's account %s", printPaymentAttrs(p.pModel))
	}
	tx.Commit() // nolint: gosec
	log.Printf("DEBUG Payment processed %s", printPaymentAttrs(p.pModel))
	return 200, nil
}

// validateSendPaymentParams method is a wrapper for validation functions.
// Accepts validation functions as an input and calls them sequentially.
func (pv *validator) validateSendPaymentParams(p *Payment, v ...func(p *Payment) (int, error)) (int, error) {
	for _, f := range v {
		code, err := f(p)
		if err != nil {
			return code, err
		}
	}
	return 200, nil
}

// validatePaymentAmount function validates that passed payment amount may be transferred and doesn't violate service's constraints.
func validatePaymentAmount(p *Payment) (int, error) {
	if *p.pModel.Amount < constants.MinBalanceValue || *p.pModel.Amount > constants.MaxBalanceValue {
		return 422, fmt.Errorf("Passed amount doesn't meet requirements: min=%.2f, max=%.2f %s", constants.MinBalanceValue, constants.MaxBalanceValue, printPaymentAttrs(p.pModel))
	}

	for k, acc := range map[string]int64{"from": *p.pModel.FromAccount, "to": *p.pModel.ToAccount} {
		balance, err := account.GetBalance(p.DB, acc)
		if err != nil {
			return 500, err
		}
		if k == "from" {
			p.pDetails.fromAccountBalance = balance
		} else {
			p.pDetails.toAccountBalance = balance
		}
	}
	if p.pDetails.fromAccountBalance < *p.pModel.Amount {
		return 422, fmt.Errorf("Insufficient funds %s", printPaymentAttrs(p.pModel))
	}

	if p.pDetails.toAccountBalance+*p.pModel.Amount > constants.MaxBalanceValue {
		// return a generic error because we don't want to disclose that someone is too rich :)
		return 422, fmt.Errorf("Payment can't be processed %s", printPaymentAttrs(p.pModel))
	}
	return 200, nil
}

// validatePaymentAccounts function validates that both accounts exist and they aren't the same.
func validatePaymentAccounts(p *Payment) (int, error) {
	if *p.pModel.FromAccount == *p.pModel.ToAccount {
		return 422, fmt.Errorf("Sender's and receiver's accounts are the same %s", printPaymentAttrs(p.pModel))
	}

	for _, acc := range []int64{*p.pModel.FromAccount, *p.pModel.ToAccount} {
		if exists := account.Exists(p.DB, acc); !exists {
			return 422, fmt.Errorf("Account doesn't exist (account=%d)", acc)
		}
	}
	return 200, nil
}

// validatePaymentCurrency function validates that both accounts are open in the same currency which matches passed payment currency.
func validatePaymentCurrency(p *Payment) (int, error) {
	paymentCurrencyID, err := account.GetCurrencyIDbyName(p.DB, p.pModel.Currency)
	if err != nil {
		return 422, err
	}

	for _, acc := range []int64{*p.pModel.FromAccount, *p.pModel.ToAccount} {
		accountCurrency, err := account.GetAccountCurrencyID(p.DB, acc)
		if err != nil {
			return 500, err
		}
		if accountCurrency == 0 {
			return 500, fmt.Errorf("Unexpected error. Failed to get account currency (account=%d)", acc)
		}
		if accountCurrency != paymentCurrencyID {
			return 422, fmt.Errorf("Payment currency doesn't match sender's or receiver's account currency %s", printPaymentAttrs(p.pModel))
		}
	}
	p.pDetails.currencyID = paymentCurrencyID
	return 200, nil
}

func setPaymentDefaultCurrency(params *models.Payment) {
	if params.Currency == "" {
		params.Currency = constants.DefaultCurrency
	}
}

// printPaymentAttrs function prints payments's details for convenient logging purpose.
func printPaymentAttrs(params *models.Payment) string {
	res := "("
	if params.ID != 0 {
		res = fmt.Sprintf("%sid=%d, ", res, params.ID)
	}
	if *params.FromAccount != 0 {
		res = fmt.Sprintf("%sfromAccount=%d, ", res, *params.FromAccount)
	}
	if *params.ToAccount != 0 {
		res = fmt.Sprintf("%stoAccount=%d, ", res, *params.ToAccount)
	}

	res = fmt.Sprintf("%samount=%.2f, ", res, *params.Amount)

	if params.Currency != "" {
		res = fmt.Sprintf("%scurrency=%s", res, params.Currency)
	}
	return fmt.Sprintf("%s)", res)
}
