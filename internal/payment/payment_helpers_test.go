package payment

import (
	"database/sql"
	"fmt"
	"testing"

	sqlmock "github.com/DATA-DOG/go-sqlmock"
	"github.com/stretchr/testify/assert"
	"gitlab.com/rtdevops/wallet-core/gen/models"
	"gitlab.com/rtdevops/wallet-core/internal/constants"
)

var pp = &Payment{}

func initDBMock(t *testing.T) sqlmock.Sqlmock {
	db, mock, err := sqlmock.New()

	if err != nil {
		t.Fatalf("an error '%s' was not expected when opening a stub database connection", err)
	}
	pp.DB = db
	return mock
}

func TestGetPaymentsHelperQueryReturnedBrokenRow(t *testing.T) {
	mock := initDBMock(t)
	defer pp.DB.Close()

	amount, fromAccount, toAccount := float64(100), int64(1), int64(2)
	testCase := &models.Payment{
		ID:          1,
		Amount:      &amount,
		Currency:    "",
		FromAccount: &fromAccount,
		ToAccount:   &toAccount,
	}

	stmt := `SELECT p\.id, p\.n_from_account_id, p\.n_to_account_id, p\.n_amount, c\.vc_name
	FROM payments   p,
			 currencies c
	WHERE p\.n_currency_id = c\.n_currency_id`

	result := mock.NewRows([]string{"id", "fromAccount", "toAccount", "amount", "currency"}).AddRow(testCase.ID, testCase.FromAccount, testCase.ToAccount, testCase.Amount, testCase.Currency).RowError(0, fmt.Errorf("error"))
	mock.ExpectQuery(stmt).WillReturnRows(result)

	res, code, err := getPaymentsHelper(pp.DB)
	assert.Equal(t, 500, code)
	assert.NotNil(t, err)
	assert.Equal(t, 0, len(res))
}

func TestGetPaymentsHelperQueryReturnedError(t *testing.T) {
	mock := initDBMock(t)
	defer pp.DB.Close()

	stmt := `SELECT p\.id, p\.n_from_account_id, p\.n_to_account_id, p\.n_amount, c\.vc_name
	FROM payments   p,
			 currencies c
	WHERE p\.n_currency_id = c\.n_currency_id`

	mock.ExpectQuery(stmt).WillReturnError(fmt.Errorf("error"))

	res, code, err := getPaymentsHelper(pp.DB)
	assert.Equal(t, 500, code)
	assert.Equal(t, fmt.Errorf("Failed to initiate DB query"), err)
	assert.Equal(t, 0, len(res))
}

func TestGetPaymentsHelperSucceed(t *testing.T) {
	mock := initDBMock(t)
	defer pp.DB.Close()

	amount, fromAccount, toAccount := float64(100), int64(1), int64(2)
	testCase := &models.Payment{
		ID:          1,
		Amount:      &amount,
		Currency:    "",
		FromAccount: &fromAccount,
		ToAccount:   &toAccount,
	}

	stmt := `SELECT p\.id, p\.n_from_account_id, p\.n_to_account_id, p\.n_amount, c\.vc_name
	FROM payments   p,
			 currencies c
	WHERE p\.n_currency_id = c\.n_currency_id`

	result := mock.NewRows([]string{"id", "fromAccount", "toAccount", "amount", "currency"})
	result.AddRow(testCase.ID, testCase.FromAccount, testCase.ToAccount, testCase.Amount, testCase.Currency)
	mock.ExpectQuery(stmt).WillReturnRows(result)

	res, code, err := getPaymentsHelper(pp.DB)
	assert.Equal(t, 200, code)
	assert.Equal(t, nil, err)
	assert.Equal(t, 1, len(res))
}

func TestSendPaymentHelperUpdateReceiversAccReturnedError(t *testing.T) {
	mock := initDBMock(t)
	defer pp.DB.Close()

	amount, fromAccount, toAccount := float64(10), int64(1), int64(2)
	testCase := &Payment{
		pModel: &models.Payment{
			ID:          1,
			Amount:      &amount,
			Currency:    "",
			FromAccount: &fromAccount,
			ToAccount:   &toAccount,
		},
		pDetails: pDetails{
			currencyID: 0,
		},
	}

	mock.ExpectBegin()
	stmtPayment := `INSERT INTO payments \(n_from_account_id, n_to_account_id, n_amount, n_currency_id\) VALUES \(\$1, \$2, \$3, \$4\) RETURNING id, n_amount`
	result := mock.NewRows([]string{"id", "amount"}).AddRow(1, *testCase.pModel.Amount)
	mock.ExpectQuery(stmtPayment).WithArgs(*testCase.pModel.FromAccount, *testCase.pModel.ToAccount, *testCase.pModel.Amount, testCase.pDetails.currencyID).WillReturnRows(result)

	stmtUpdateSendersAcc := `UPDATE accounts SET n_balance = n_balance - \$1 WHERE id = \$2`
	mock.ExpectExec(stmtUpdateSendersAcc).WithArgs(*testCase.pModel.Amount, *testCase.pModel.FromAccount).WillReturnResult(sqlmock.NewResult(1, 1))

	stmtUpdateReceiverAcc := `UPDATE accounts SET n_balance = n_balance \+ \$1 WHERE id = \$2`
	mock.ExpectExec(stmtUpdateReceiverAcc).WithArgs(*testCase.pModel.Amount, *testCase.pModel.ToAccount).WillReturnError(fmt.Errorf("error"))

	mock.ExpectRollback()

	mockObj := mockSendPaymentHelperSucceed{}
	code, err := sendPaymentHelper(pp.DB, &mockObj, testCase.pModel)
	assert.Equal(t, 500, code)
	assert.Equal(t, fmt.Errorf("Unexpected error. Failed to add funds to receiver's account %s", printPaymentAttrs(testCase.pModel)), err)
}

func TestSendPaymentHelperUpdateSenderAccReturnedError(t *testing.T) {
	mock := initDBMock(t)
	defer pp.DB.Close()

	amount, fromAccount, toAccount := float64(10), int64(1), int64(2)
	testCase := &Payment{
		pModel: &models.Payment{
			ID:          1,
			Amount:      &amount,
			Currency:    "",
			FromAccount: &fromAccount,
			ToAccount:   &toAccount,
		},
		pDetails: pDetails{
			currencyID: 0,
		},
	}

	mock.ExpectBegin()
	stmtPayment := `INSERT INTO payments \(n_from_account_id, n_to_account_id, n_amount, n_currency_id\) VALUES \(\$1, \$2, \$3, \$4\) RETURNING id, n_amount`
	result := mock.NewRows([]string{"id", "amount"}).AddRow(1, *testCase.pModel.Amount)
	mock.ExpectQuery(stmtPayment).WithArgs(*testCase.pModel.FromAccount, *testCase.pModel.ToAccount, *testCase.pModel.Amount, testCase.pDetails.currencyID).WillReturnRows(result)

	stmtUpdateSendersAcc := `UPDATE accounts SET n_balance = n_balance - \$1 WHERE id = \$2`
	mock.ExpectExec(stmtUpdateSendersAcc).WithArgs(*testCase.pModel.Amount, *testCase.pModel.FromAccount).WillReturnError(fmt.Errorf("error"))
	mock.ExpectRollback()

	mockObj := mockSendPaymentHelperSucceed{}
	code, err := sendPaymentHelper(pp.DB, &mockObj, testCase.pModel)
	assert.Equal(t, 500, code)
	assert.Equal(t, fmt.Errorf("Unexpected error. Failed to charge sender's account %s", printPaymentAttrs(testCase.pModel)), err)
}

func TestSendPaymentHelperInsertPaymentReturnedUnexpectedError(t *testing.T) {
	mock := initDBMock(t)
	defer pp.DB.Close()

	amount, fromAccount, toAccount := float64(10), int64(1), int64(2)
	testCase := &Payment{
		pModel: &models.Payment{
			ID:          1,
			Amount:      &amount,
			Currency:    "",
			FromAccount: &fromAccount,
			ToAccount:   &toAccount,
		},
		pDetails: pDetails{
			currencyID: 0,
		},
	}

	mock.ExpectBegin()
	stmtPayment := `INSERT INTO payments \(n_from_account_id, n_to_account_id, n_amount, n_currency_id\) VALUES \(\$1, \$2, \$3, \$4\) RETURNING id, n_amount`
	mock.ExpectQuery(stmtPayment).WithArgs(*testCase.pModel.FromAccount, *testCase.pModel.ToAccount, *testCase.pModel.Amount, testCase.pDetails.currencyID)
	mock.ExpectRollback()

	mockObj := mockSendPaymentHelperSucceed{}
	code, err := sendPaymentHelper(pp.DB, &mockObj, testCase.pModel)
	assert.Equal(t, 500, code)
	assert.Equal(t, fmt.Errorf("Unexpected error. Failed to process payment %s", printPaymentAttrs(testCase.pModel)), err)
}

func TestSendPaymentHelperInsertPaymentReturnedNoRows(t *testing.T) {
	mock := initDBMock(t)
	defer pp.DB.Close()

	amount, fromAccount, toAccount := float64(10), int64(1), int64(2)
	testCase := &Payment{
		pModel: &models.Payment{
			ID:          1,
			Amount:      &amount,
			Currency:    "",
			FromAccount: &fromAccount,
			ToAccount:   &toAccount,
		},
		pDetails: pDetails{
			currencyID: 0,
		},
	}

	mock.ExpectBegin()
	stmtPayment := `INSERT INTO payments \(n_from_account_id, n_to_account_id, n_amount, n_currency_id\) VALUES \(\$1, \$2, \$3, \$4\) RETURNING id, n_amount`
	mock.ExpectQuery(stmtPayment).WithArgs(*testCase.pModel.FromAccount, *testCase.pModel.ToAccount, *testCase.pModel.Amount, testCase.pDetails.currencyID).WillReturnError(sql.ErrNoRows)
	mock.ExpectRollback()

	mockObj := mockSendPaymentHelperSucceed{}
	code, err := sendPaymentHelper(pp.DB, &mockObj, testCase.pModel)
	assert.Equal(t, 500, code)
	assert.Equal(t, fmt.Errorf("Failed to process payment, no payment ID generated %s", printPaymentAttrs(testCase.pModel)), err)
}

type mockSendPaymentHelperNewTransactionFailed struct{}

func (mock *mockSendPaymentHelperNewTransactionFailed) validateSendPaymentParams(p *Payment, v ...func(p *Payment) (int, error)) (int, error) {
	return 200, nil
}

func TestSendPaymentHelperNewTransactionFailed(t *testing.T) {
	mock := initDBMock(t)
	defer pp.DB.Close()

	amount, fromAccount, toAccount := float64(10), int64(1), int64(2)
	testCase := &models.Payment{
		ID:          1,
		Amount:      &amount,
		Currency:    "",
		FromAccount: &fromAccount,
		ToAccount:   &toAccount,
	}

	mock.ExpectBegin().WillReturnError(fmt.Errorf("error"))

	mockObj := mockSendPaymentHelperNewTransactionFailed{}
	code, err := sendPaymentHelper(pp.DB, &mockObj, testCase)
	assert.Equal(t, 500, code)
	assert.Equal(t, fmt.Errorf("Failed to initiate new DB transaction %s", printPaymentAttrs(testCase)), err)
}

type mockSendPaymentHelperValidatorFailed struct{}

func (mock *mockSendPaymentHelperValidatorFailed) validateSendPaymentParams(p *Payment, v ...func(p *Payment) (int, error)) (int, error) {
	return 500, fmt.Errorf("error")
}

func TestSendPaymentHelperValidatorFailed(t *testing.T) {
	amount, fromAccount, toAccount := float64(10), int64(1), int64(2)
	testCase := &models.Payment{
		ID:          1,
		Amount:      &amount,
		Currency:    "",
		FromAccount: &fromAccount,
		ToAccount:   &toAccount,
	}

	mockObj := mockSendPaymentHelperValidatorFailed{}
	code, err := sendPaymentHelper(pp.DB, &mockObj, testCase)
	assert.Equal(t, 500, code)
	assert.Equal(t, fmt.Errorf("error"), err)
}

type mockSendPaymentHelperSucceed struct{}

func (mock *mockSendPaymentHelperSucceed) validateSendPaymentParams(p *Payment, v ...func(p *Payment) (int, error)) (int, error) {
	return 200, nil
}

func TestSendPaymentHelperSucceed(t *testing.T) {
	mock := initDBMock(t)
	defer pp.DB.Close()

	amount, fromAccount, toAccount := float64(10), int64(1), int64(2)
	testCase := &Payment{
		pModel: &models.Payment{
			ID:          1,
			Amount:      &amount,
			Currency:    "",
			FromAccount: &fromAccount,
			ToAccount:   &toAccount,
		},
		pDetails: pDetails{
			currencyID: 0,
		},
	}

	mock.ExpectBegin()
	stmtPayment := `INSERT INTO payments \(n_from_account_id, n_to_account_id, n_amount, n_currency_id\) VALUES \(\$1, \$2, \$3, \$4\) RETURNING id, n_amount`
	result := mock.NewRows([]string{"id", "amount"}).AddRow(1, *testCase.pModel.Amount)
	mock.ExpectQuery(stmtPayment).WithArgs(*testCase.pModel.FromAccount, *testCase.pModel.ToAccount, *testCase.pModel.Amount, testCase.pDetails.currencyID).WillReturnRows(result)

	stmtUpdateSendersAcc := `UPDATE accounts SET n_balance = n_balance - \$1 WHERE id = \$2`
	mock.ExpectExec(stmtUpdateSendersAcc).WithArgs(*testCase.pModel.Amount, *testCase.pModel.FromAccount).WillReturnResult(sqlmock.NewResult(1, 1))

	stmtUpdateReceiverAcc := `UPDATE accounts SET n_balance = n_balance \+ \$1 WHERE id = \$2`
	mock.ExpectExec(stmtUpdateReceiverAcc).WithArgs(*testCase.pModel.Amount, *testCase.pModel.ToAccount).WillReturnResult(sqlmock.NewResult(1, 1))

	mock.ExpectCommit()

	mockObj := mockSendPaymentHelperSucceed{}
	code, err := sendPaymentHelper(pp.DB, &mockObj, testCase.pModel)
	assert.Equal(t, 200, code)
	assert.Equal(t, nil, err)
}

func TestSetPaymentDefaultCurrencyNoCurrencyPassed(t *testing.T) {
	testCase := &models.Payment{
		Currency: "",
	}

	setPaymentDefaultCurrency(testCase)
	assert.Equal(t, constants.DefaultCurrency, testCase.Currency)
}

func TestSetPaymentDefaultCurrencyCurrencyPassed(t *testing.T) {
	testCase := &models.Payment{
		Currency: "euro",
	}

	setPaymentDefaultCurrency(testCase)
	assert.Equal(t, "euro", testCase.Currency)
}

func TestValidateSendPaymentParamsSucceed(t *testing.T) {
	testCase := &Payment{pModel: &models.Payment{}}
	f := func(p *Payment) (int, error) {
		return 200, nil
	}

	v := &validator{}
	code, err := v.validateSendPaymentParams(testCase, f)
	assert.Equal(t, 200, code)
	assert.Equal(t, nil, err)
}

func TestValidateSendPaymentParamsAnyFunctionFailed(t *testing.T) {
	testCase := &Payment{pModel: &models.Payment{}}
	f := func(p *Payment) (int, error) {
		return 422, fmt.Errorf("error")
	}

	v := &validator{}
	code, err := v.validateSendPaymentParams(testCase, f)
	assert.Equal(t, 422, code)
	assert.Equal(t, fmt.Errorf("error"), err)
}

func TestValidatePaymentAmountSucceed(t *testing.T) {
	mock := initDBMock(t)
	defer pp.DB.Close()

	amount, fromAccount, toAccount := float64(10), int64(1), int64(2)
	testCase := &Payment{
		DB: pp.DB,
		pModel: &models.Payment{
			ID:          1,
			Amount:      &amount,
			Currency:    "us dollar",
			FromAccount: &fromAccount,
			ToAccount:   &toAccount,
		},
	}

	stmtGetBalance := `SELECT n_balance FROM accounts WHERE id = \$1`

	result := mock.NewRows([]string{"balance"}).AddRow(*testCase.pModel.Amount + 1)
	mock.ExpectQuery(stmtGetBalance).WithArgs(testCase.pModel.FromAccount).WillReturnRows(result)

	result = mock.NewRows([]string{"balance"}).AddRow(0)
	mock.ExpectQuery(stmtGetBalance).WithArgs(testCase.pModel.ToAccount).WillReturnRows(result)

	code, err := validatePaymentAmount(testCase)
	assert.Equal(t, 200, code)
	assert.Equal(t, nil, err)
}

func TestValidatePaymentAmountAmountLesserThanMinConst(t *testing.T) {
	amount, fromAccount, toAccount := float64(constants.MinBalanceValue-0.01), int64(1), int64(1)
	testCase := &Payment{
		pModel: &models.Payment{
			ID:          1,
			Amount:      &amount,
			Currency:    "us dollar",
			FromAccount: &fromAccount,
			ToAccount:   &toAccount,
		},
	}

	code, err := validatePaymentAmount(testCase)
	assert.Equal(t, 422, code)
	assert.Equal(t, fmt.Errorf("Passed amount doesn't meet requirements: min=%.2f, max=%.2f %s", constants.MinBalanceValue, constants.MaxBalanceValue, printPaymentAttrs(testCase.pModel)), err)
}

func TestValidatePaymentAmountAmountGreaterThanMaxConst(t *testing.T) {
	amount, fromAccount, toAccount := float64(constants.MaxBalanceValue+0.01), int64(1), int64(1)
	testCase := &Payment{
		pModel: &models.Payment{
			ID:          1,
			Amount:      &amount,
			Currency:    "us dollar",
			FromAccount: &fromAccount,
			ToAccount:   &toAccount,
		},
	}

	code, err := validatePaymentAmount(testCase)
	assert.Equal(t, 422, code)
	assert.Equal(t, fmt.Errorf("Passed amount doesn't meet requirements: min=%.2f, max=%.2f %s", constants.MinBalanceValue, constants.MaxBalanceValue, printPaymentAttrs(testCase.pModel)), err)
}

func TestValidatePaymentAmountFailedToGetEitherSendersOrReceiversBalance(t *testing.T) {
	mock := initDBMock(t)
	defer pp.DB.Close()

	amount, fromAccount, toAccount := float64(10), int64(1), int64(1)
	testCase := &Payment{
		DB: pp.DB,
		pModel: &models.Payment{
			ID:          1,
			Amount:      &amount,
			Currency:    "us dollar",
			FromAccount: &fromAccount,
			ToAccount:   &toAccount,
		},
	}

	stmtGetBalance := `SELECT n_balance FROM accounts WHERE id = \$1`

	mock.ExpectQuery(stmtGetBalance).WithArgs(testCase.pModel.FromAccount)

	code, err := validatePaymentAmount(testCase)
	assert.Equal(t, 500, code)
	assert.Equal(t, fmt.Errorf("Unexpected error. Failed to get account balance (account=1)"), err)
}

func TestValidatePaymentAmountSendersInsufficientFunds(t *testing.T) {
	mock := initDBMock(t)
	defer pp.DB.Close()

	amount, fromAccount, toAccount := float64(100), int64(1), int64(1)
	testCase := &Payment{
		DB: pp.DB,
		pModel: &models.Payment{
			ID:          1,
			Amount:      &amount,
			Currency:    "us dollar",
			FromAccount: &fromAccount,
			ToAccount:   &toAccount,
		},
	}

	stmtGetBalance := `SELECT n_balance FROM accounts WHERE id = \$1`

	result := mock.NewRows([]string{"balance"}).AddRow(0)
	mock.ExpectQuery(stmtGetBalance).WithArgs(testCase.pModel.FromAccount).WillReturnRows(result)

	result = mock.NewRows([]string{"balance"}).AddRow(0)
	mock.ExpectQuery(stmtGetBalance).WithArgs(testCase.pModel.ToAccount).WillReturnRows(result)

	code, err := validatePaymentAmount(testCase)
	assert.Equal(t, 422, code)
	assert.Equal(t, fmt.Errorf("Insufficient funds %s", printPaymentAttrs(testCase.pModel)), err)
}

func TestValidatePaymentAmountReceiversBalanceExceedsMaxConst(t *testing.T) {
	mock := initDBMock(t)
	defer pp.DB.Close()

	amount, fromAccount, toAccount := float64(100), int64(100), int64(1)
	testCase := &Payment{
		DB: pp.DB,
		pModel: &models.Payment{
			ID:          1,
			Amount:      &amount,
			Currency:    "us dollar",
			FromAccount: &fromAccount,
			ToAccount:   &toAccount,
		},
	}

	stmtGetBalance := `SELECT n_balance FROM accounts WHERE id = \$1`

	result := mock.NewRows([]string{"balance"}).AddRow(testCase.pModel.Amount)
	mock.ExpectQuery(stmtGetBalance).WithArgs(testCase.pModel.FromAccount).WillReturnRows(result)

	result = mock.NewRows([]string{"balance"}).AddRow(constants.MaxBalanceValue)
	mock.ExpectQuery(stmtGetBalance).WithArgs(testCase.pModel.ToAccount).WillReturnRows(result)

	code, err := validatePaymentAmount(testCase)
	assert.Equal(t, 422, code)
	assert.Equal(t, fmt.Errorf("Payment can't be processed %s", printPaymentAttrs(testCase.pModel)), err)
}

func TestValidatePaymentCurrencySucceed(t *testing.T) {
	mock := initDBMock(t)
	defer pp.DB.Close()

	amount, fromAccount, toAccount := float64(10), int64(1), int64(1)
	testCase := &Payment{
		DB: pp.DB,
		pModel: &models.Payment{
			ID:          1,
			Amount:      &amount,
			Currency:    "us dollar",
			FromAccount: &fromAccount,
			ToAccount:   &toAccount,
		},
	}

	stmtGetCurrencyIDbyName := `SELECT n_currency_id FROM currencies WHERE trim\(lower\(vc_name\)\) = trim\(lower\(\$1\)\)`
	result := mock.NewRows([]string{"id"}).AddRow(1)
	mock.ExpectQuery(stmtGetCurrencyIDbyName).WithArgs(testCase.pModel.Currency).WillReturnRows(result)

	stmtGetAccountCurrencyID := `SELECT n_currency_id FROM accounts WHERE id = \$1`
	result = mock.NewRows([]string{"id"}).AddRow(1)
	mock.ExpectQuery(stmtGetAccountCurrencyID).WithArgs(testCase.pModel.FromAccount).WillReturnRows(result)

	result = mock.NewRows([]string{"id"}).AddRow(1)
	mock.ExpectQuery(stmtGetAccountCurrencyID).WithArgs(testCase.pModel.ToAccount).WillReturnRows(result)

	code, err := validatePaymentCurrency(testCase)
	assert.Equal(t, 200, code)
	assert.Equal(t, nil, err)
}

func TestValidatePaymentCurrencyPaymentCurrencyNotFound(t *testing.T) {
	mock := initDBMock(t)
	defer pp.DB.Close()

	amount, fromAccount, toAccount := float64(10), int64(1), int64(1)
	testCase := &Payment{
		DB: pp.DB,
		pModel: &models.Payment{
			ID:          1,
			Amount:      &amount,
			Currency:    "chinese yuan",
			FromAccount: &fromAccount,
			ToAccount:   &toAccount,
		},
	}

	stmtGetCurrencyIDbyName := `SELECT n_currency_id FROM currencies WHERE trim\(lower\(vc_name\)\) = trim\(lower\(\$1\)\)`
	mock.ExpectQuery(stmtGetCurrencyIDbyName).WithArgs(testCase.pModel.Currency)

	code, err := validatePaymentCurrency(testCase)
	assert.Equal(t, 422, code)
	assert.Equal(t, fmt.Errorf("Currency name not found (currency=chinese yuan)"), err)
}

func TestValidatePaymentCurrencySendersOrReceiversCurrencyNotFound(t *testing.T) {
	mock := initDBMock(t)
	defer pp.DB.Close()

	amount, fromAccount, toAccount := float64(10), int64(1), int64(1)
	testCase := &Payment{
		DB: pp.DB,
		pModel: &models.Payment{
			ID:          1,
			Amount:      &amount,
			Currency:    "us dollar",
			FromAccount: &fromAccount,
			ToAccount:   &toAccount,
		},
	}

	stmtGetCurrencyIDbyName := `SELECT n_currency_id FROM currencies WHERE trim\(lower\(vc_name\)\) = trim\(lower\(\$1\)\)`
	result := mock.NewRows([]string{"id"}).AddRow(1)
	mock.ExpectQuery(stmtGetCurrencyIDbyName).WithArgs(testCase.pModel.Currency).WillReturnRows(result)

	stmtGetAccountCurrencyID := `SELECT n_currency_id FROM accounts WHERE id = \$1`
	mock.ExpectQuery(stmtGetAccountCurrencyID).WithArgs(testCase.pModel.FromAccount)

	code, err := validatePaymentCurrency(testCase)
	assert.Equal(t, 500, code)
	assert.Equal(t, fmt.Errorf("Unexpected error. Account currency not found (account=1)"), err)
}

func TestValidatePaymentCurrencySendersOrReceiversCurrencyIdIsZero(t *testing.T) {
	mock := initDBMock(t)
	defer pp.DB.Close()

	amount, fromAccount, toAccount := float64(10), int64(1), int64(1)
	testCase := &Payment{
		DB: pp.DB,
		pModel: &models.Payment{
			ID:          1,
			Amount:      &amount,
			Currency:    "us dollar",
			FromAccount: &fromAccount,
			ToAccount:   &toAccount,
		},
	}

	stmtGetCurrencyIDbyName := `SELECT n_currency_id FROM currencies WHERE trim\(lower\(vc_name\)\) = trim\(lower\(\$1\)\)`
	result := mock.NewRows([]string{"id"}).AddRow(1)
	mock.ExpectQuery(stmtGetCurrencyIDbyName).WithArgs(testCase.pModel.Currency).WillReturnRows(result)

	stmtGetAccountCurrencyID := `SELECT n_currency_id FROM accounts WHERE id = \$1`
	result = mock.NewRows([]string{"id"}).AddRow(0)
	mock.ExpectQuery(stmtGetAccountCurrencyID).WithArgs(testCase.pModel.FromAccount).WillReturnRows(result)

	code, err := validatePaymentCurrency(testCase)
	assert.Equal(t, 500, code)
	assert.Equal(t, fmt.Errorf("Unexpected error. Failed to get account currency (account=%d)", *testCase.pModel.FromAccount), err)
}

func TestValidatePaymentCurrencySendersOrReceiversCurrencyDoesntMatchPaymentCurrency(t *testing.T) {
	mock := initDBMock(t)
	defer pp.DB.Close()

	amount, fromAccount, toAccount := float64(10), int64(1), int64(1)
	testCase := &Payment{
		DB: pp.DB,
		pModel: &models.Payment{
			ID:          1,
			Amount:      &amount,
			Currency:    "us dollar",
			FromAccount: &fromAccount,
			ToAccount:   &toAccount,
		},
	}

	stmtGetCurrencyIDbyName := `SELECT n_currency_id FROM currencies WHERE trim\(lower\(vc_name\)\) = trim\(lower\(\$1\)\)`
	result := mock.NewRows([]string{"id"}).AddRow(1)
	mock.ExpectQuery(stmtGetCurrencyIDbyName).WithArgs(testCase.pModel.Currency).WillReturnRows(result)

	stmtGetAccountCurrencyID := `SELECT n_currency_id FROM accounts WHERE id = \$1`
	result = mock.NewRows([]string{"id"}).AddRow(1)
	mock.ExpectQuery(stmtGetAccountCurrencyID).WithArgs(testCase.pModel.FromAccount).WillReturnRows(result)

	result = mock.NewRows([]string{"id"}).AddRow(2)
	mock.ExpectQuery(stmtGetAccountCurrencyID).WithArgs(testCase.pModel.ToAccount).WillReturnRows(result)

	code, err := validatePaymentCurrency(testCase)
	assert.Equal(t, 422, code)
	assert.Equal(t, fmt.Errorf("Payment currency doesn't match sender's or receiver's account currency %s", printPaymentAttrs(testCase.pModel)), err)
}

func TestValidatePaymentAccountsSenderSameAsReceiver(t *testing.T) {
	fromAccount, toAccount, amount := int64(1), int64(1), float64(10)
	testCase := &Payment{
		pModel: &models.Payment{
			FromAccount: &fromAccount,
			ToAccount:   &toAccount,
			Amount:      &amount,
		},
	}

	code, err := validatePaymentAccounts(testCase)
	assert.Equal(t, 422, code)
	assert.Equal(t, fmt.Errorf("Sender's and receiver's accounts are the same %s", printPaymentAttrs(testCase.pModel)), err)
}

func TestValidatePaymentAccountsSenderOrReceiverNotExists(t *testing.T) {
	mock := initDBMock(t)
	defer pp.DB.Close()

	fromAccount, toAccount := int64(1), int64(2)
	testCase := &Payment{
		DB: pp.DB,
		pModel: &models.Payment{
			FromAccount: &fromAccount,
			ToAccount:   &toAccount,
		},
	}

	stmt := `SELECT COUNT\(id\) FROM accounts WHERE id = \$1`
	result := mock.NewRows([]string{"count"})
	if *testCase.pModel.FromAccount == 2 {
		result.AddRow(1)
	}
	mock.ExpectQuery(stmt).WithArgs(1).WillReturnRows(result)

	code, err := validatePaymentAccounts(testCase)
	assert.Equal(t, 422, code)
	assert.Equal(t, fmt.Errorf("Account doesn't exist (account=1)"), err)
}

func TestValidatePaymentAccountsSucceed(t *testing.T) {
	mock := initDBMock(t)
	defer pp.DB.Close()

	fromAccount, toAccount := int64(2), int64(1)
	testCase := &Payment{
		DB: pp.DB,
		pModel: &models.Payment{
			FromAccount: &fromAccount,
			ToAccount:   &toAccount,
		},
	}

	stmt := `SELECT COUNT\(id\) FROM accounts WHERE id = \$1`

	result := mock.NewRows([]string{"count"}).AddRow(1)
	mock.ExpectQuery(stmt).WithArgs(2).WillReturnRows(result)

	result = mock.NewRows([]string{"count"}).AddRow(1)
	mock.ExpectQuery(stmt).WithArgs(1).WillReturnRows(result)

	code, err := validatePaymentAccounts(testCase)
	assert.Equal(t, 200, code)
	assert.Equal(t, nil, err)
}
