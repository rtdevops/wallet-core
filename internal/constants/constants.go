// Package constants defines constants that are used by other packages.
package constants

const (
	// MaxBalanceValue is a maximum possible amount for one payment and account balance. If account's balance reached that constant, new incoming payments will be rejected.
	MaxBalanceValue = 10000000000000.99
	// MinBalanceValue is a minimum possible amount that can be transferred.
	MinBalanceValue = 0.01
	// DefaultCurrency is default currency that is used if user didn't specify any currency in the request.
	DefaultCurrency = "US Dollar"
)
