// Package account includes types and methods to handle /accounts and /account/* requests.
package account

import (
	"context"
	"database/sql"

	"github.com/go-openapi/runtime/middleware"
	"github.com/go-openapi/swag"
	log "github.com/go-pkgz/lgr"
	"gitlab.com/rtdevops/wallet-core/gen/models"
	"gitlab.com/rtdevops/wallet-core/gen/restapi/operations/account"
)

// Account type stores DB connection for package functions and methods.
// It may be extended to store any data needed to handle requests.
type Account struct {
	DB *sql.DB
}

// CreateAccount method is an entrypoint for /account/create requests.
func (a *Account) CreateAccount(ctx context.Context, params account.CreateAccountParams) middleware.Responder {
	code, err := createAccountHelper(a.DB, params.Account)
	if err != nil {
		if code >= 400 && code < 500 {
			log.Printf("WARN %s %s", err, printPaymentAttrs(params.Account))
		} else {
			log.Printf("ERROR %s %s", err, printPaymentAttrs(params.Account))
		}
		return account.NewCreateAccountDefault(code).WithPayload(&models.Error{Message: swag.String(err.Error())})
	}
	return account.NewCreateAccountOK().WithPayload(params.Account)
}

// GetAccounts method is an entrypoint for /accounts requests.
func (a *Account) GetAccounts(ctx context.Context, params account.GetAccountsParams) middleware.Responder {
	accounts, code, err := getAccountsHelper(a.DB)
	if err != nil {
		if code >= 400 && code < 500 {
			log.Printf("WARN %s", err)
		} else {
			log.Printf("ERROR %s", err)
		}
		return account.NewGetAccountsDefault(code).WithPayload(&models.Error{Message: swag.String(err.Error())})
	}
	return account.NewGetAccountsOK().WithPayload(accounts)
}
