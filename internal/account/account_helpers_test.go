package account

import (
	"database/sql"
	"fmt"
	"testing"

	sqlmock "github.com/DATA-DOG/go-sqlmock"
	"github.com/stretchr/testify/assert"
	"gitlab.com/rtdevops/wallet-core/gen/models"
	"gitlab.com/rtdevops/wallet-core/internal/constants"
)

var acc = &Account{}

func initDBMock(t *testing.T) sqlmock.Sqlmock {
	db, mock, err := sqlmock.New()

	if err != nil {
		t.Fatalf("an error '%s' was not expected when opening a stub database connection", err)
	}
	acc.DB = db
	return mock
}

func TestCreateAccountHelperSucceed(t *testing.T) {
	mock := initDBMock(t)
	defer acc.DB.Close()

	testCase := &models.Account{
		ID:       1,
		Currency: "us dollar",
		Balance:  100,
	}

	stmtGetCurrencyIDbyName := `SELECT n_currency_id FROM currencies WHERE trim\(lower\(vc_name\)\) = trim\(lower\(\$1\)\)`
	result := mock.NewRows([]string{"id"}).AddRow(1)
	mock.ExpectQuery(stmtGetCurrencyIDbyName).WithArgs(testCase.Currency).WillReturnRows(result)

	stmtInsertIntoAccounts := `INSERT INTO accounts \(n_currency_id, n_balance\) VALUES \(\$1, \$2\) RETURNING id, n_balance`
	result = mock.NewRows([]string{"id", "balance"}).AddRow(testCase.ID, testCase.Balance)
	mock.ExpectQuery(stmtInsertIntoAccounts).WithArgs(1, testCase.Balance).WillReturnRows(result)

	code, err := createAccountHelper(acc.DB, testCase)
	assert.Equal(t, 200, code)
	assert.Equal(t, nil, err)
}

func TestCreateAccountHelperFailedOnAccStartBelowZero(t *testing.T) {
	testCase := &models.Account{
		ID:       1,
		Currency: "us dollar",
		Balance:  -1,
	}

	code, err := createAccountHelper(acc.DB, testCase)
	assert.Equal(t, 422, code)
	assert.Equal(t, fmt.Errorf("Account start balance is invalid (account=%d, balance=%.2f)", testCase.ID, testCase.Balance), err)
}

func TestCreateAccountHelperFailedOnAccStartExceedsMax(t *testing.T) {
	testCase := &models.Account{
		ID:       1,
		Currency: "us dollar",
		Balance:  constants.MaxBalanceValue + 1,
	}

	code, err := createAccountHelper(acc.DB, testCase)
	assert.Equal(t, 422, code)
	assert.Equal(t, fmt.Errorf("Account start balance is invalid (account=%d, balance=%.2f)", testCase.ID, testCase.Balance), err)
}

func TestCreateAccountHelperFailedOnCurrencyNotFound(t *testing.T) {
	mock := initDBMock(t)
	defer acc.DB.Close()

	testCase := &models.Account{
		ID:       1,
		Currency: "chinese yuan",
		Balance:  100,
	}

	stmtGetCurrencyIDbyName := `SELECT n_currency_id FROM currencies WHERE trim\(lower\(vc_name\)\) = trim\(lower\(\$1\)\)`
	result := mock.NewRows([]string{"id"}).AddRow(0)
	mock.ExpectQuery(stmtGetCurrencyIDbyName).WithArgs(testCase.Currency).WillReturnRows(result)

	code, err := createAccountHelper(acc.DB, testCase)
	assert.Equal(t, 422, code)
	assert.Equal(t, fmt.Errorf("Currency name not found (currency=chinese yuan)"), err)
}

func TestCreateAccountHelperFailedOnAccInsertReturnedNoRows(t *testing.T) {
	mock := initDBMock(t)
	defer acc.DB.Close()

	testCase := &models.Account{
		ID:       1,
		Currency: "us dollar",
		Balance:  100,
	}

	stmtGetCurrencyIDbyName := `SELECT n_currency_id FROM currencies WHERE trim\(lower\(vc_name\)\) = trim\(lower\(\$1\)\)`
	result := mock.NewRows([]string{"id"}).AddRow(1)
	mock.ExpectQuery(stmtGetCurrencyIDbyName).WithArgs(testCase.Currency).WillReturnRows(result)

	stmtInsertIntoAccounts := `INSERT INTO accounts \(n_currency_id, n_balance\) VALUES \(\$1, \$2\) RETURNING id`
	mock.ExpectQuery(stmtInsertIntoAccounts).WithArgs(1, testCase.Balance).WillReturnError(sql.ErrNoRows)

	code, err := createAccountHelper(acc.DB, testCase)
	assert.Equal(t, 500, code)
	assert.Equal(t, fmt.Errorf("Failed to create account %s", printPaymentAttrs(testCase)), err)
}

func TestCreateAccountHelperFailedOnAccInsertReturnedBrokenRow(t *testing.T) {
	mock := initDBMock(t)
	defer acc.DB.Close()

	testCase := &models.Account{
		ID:       1,
		Currency: "us dollar",
		Balance:  100,
	}

	stmtGetCurrencyIDbyName := `SELECT n_currency_id FROM currencies WHERE trim\(lower\(vc_name\)\) = trim\(lower\(\$1\)\)`
	result := mock.NewRows([]string{"id"}).AddRow(1)
	mock.ExpectQuery(stmtGetCurrencyIDbyName).WithArgs(testCase.Currency).WillReturnRows(result)

	stmtInsertIntoAccounts := `INSERT INTO accounts \(n_currency_id, n_balance\) VALUES \(\$1, \$2\) RETURNING id`
	result = mock.NewRows([]string{"id"}).AddRow(testCase.ID).RowError(0, fmt.Errorf("error"))
	mock.ExpectQuery(stmtInsertIntoAccounts).WithArgs(1, testCase.Balance).WillReturnRows(result)

	code, err := createAccountHelper(acc.DB, testCase)
	assert.Equal(t, 500, code)
	assert.Equal(t, fmt.Errorf("Unexpected error. Failed to create account %s", printPaymentAttrs(testCase)), err)
}

func TestGetAccountsHelperSucceed(t *testing.T) {
	mock := initDBMock(t)
	defer acc.DB.Close()

	testCase := &models.Account{
		ID:       1,
		Currency: "us dollar",
		Balance:  100,
	}

	stmt := `SELECT a\.id, a\.n_balance, c\.vc_name
	FROM accounts   a,
			 currencies c
	WHERE a\.n_currency_id = c\.n_currency_id`

	result := mock.NewRows([]string{"id", "balance", "currency"}).AddRow(testCase.ID, testCase.Balance, testCase.Currency)
	mock.ExpectQuery(stmt).WillReturnRows(result)

	res, code, err := getAccountsHelper(acc.DB)
	assert.Equal(t, 200, code)
	assert.Equal(t, nil, err)
	assert.Equal(t, 1, len(res))
}

func TestGetAccountsHelperFailedOnSelectReturnedError(t *testing.T) {
	mock := initDBMock(t)
	defer acc.DB.Close()

	stmt := `SELECT a\.id, a\.n_balance, c\.vc_name
	FROM accounts   a,
			 currencies c
	WHERE a\.n_currency_id = c\.n_currency_id`

	mock.ExpectQuery(stmt).WillReturnError(sql.ErrConnDone)

	res, code, err := getAccountsHelper(acc.DB)
	assert.Equal(t, 500, code)
	assert.NotNil(t, err)
	assert.Equal(t, 0, len(res))
}

func TestGetAccountsHelperFailedOnSelectReturnedBrokenRow(t *testing.T) {
	mock := initDBMock(t)
	defer acc.DB.Close()

	testCase := &models.Account{
		ID:       1,
		Currency: "us dollar",
		Balance:  100,
	}

	stmt := `SELECT a\.id, a\.n_balance, c\.vc_name
	FROM accounts   a,
			 currencies c
	WHERE a\.n_currency_id = c\.n_currency_id`

	result := mock.NewRows([]string{"id", "balance", "currency"}).AddRow(testCase.ID, testCase.Balance, testCase.Currency).RowError(0, fmt.Errorf("error"))
	mock.ExpectQuery(stmt).WillReturnRows(result)

	res, code, err := getAccountsHelper(acc.DB)
	assert.Equal(t, 500, code)
	assert.NotNil(t, err)
	assert.Equal(t, 0, len(res))
}

func TestGetCurrencyIDByNameSucceed(t *testing.T) {
	mock := initDBMock(t)
	defer acc.DB.Close()

	testCase := &models.Account{
		ID:       1,
		Currency: "us dollar",
		Balance:  100,
	}

	stmt := `SELECT n_currency_id FROM currencies WHERE trim\(lower\(vc_name\)\) = trim\(lower\(\$1\)\)`
	result := mock.NewRows([]string{"id"}).AddRow(1)
	mock.ExpectQuery(stmt).WithArgs(testCase.Currency).WillReturnRows(result)

	id, err := GetCurrencyIDbyName(acc.DB, testCase.Currency)
	assert.Equal(t, 1, id)
	assert.Equal(t, nil, err)
}

func TestGetCurrencyIDByNameFailedOnSelectReturnedBrokenRow(t *testing.T) {
	mock := initDBMock(t)
	defer acc.DB.Close()

	testCase := &models.Account{
		ID:       1,
		Currency: "us dollar",
		Balance:  100,
	}

	stmt := `SELECT n_currency_id FROM currencies WHERE trim\(lower\(vc_name\)\) = trim\(lower\(\$1\)\)`
	result := mock.NewRows([]string{"id"}).AddRow(1).RowError(0, fmt.Errorf("error"))
	mock.ExpectQuery(stmt).WithArgs(testCase.Currency).WillReturnRows(result)

	id, err := GetCurrencyIDbyName(acc.DB, testCase.Currency)
	assert.Equal(t, 0, id)
	assert.Equal(t, fmt.Errorf("Currency name not found (currency=%s)", testCase.Currency), err)
}

func TestGetAccountCurrencyIDSucceed(t *testing.T) {
	mock := initDBMock(t)
	defer acc.DB.Close()

	testCase := &models.Account{
		ID:       1,
		Currency: "us dollar",
		Balance:  100,
	}

	stmt := `SELECT n_currency_id FROM accounts WHERE id = \$1`
	result := mock.NewRows([]string{"id"}).AddRow(1)
	mock.ExpectQuery(stmt).WithArgs(testCase.ID).WillReturnRows(result)

	id, err := GetAccountCurrencyID(acc.DB, testCase.ID)
	assert.Equal(t, 1, id)
	assert.Equal(t, nil, err)
}

func TestGetAccountCurrencyIDFailedOnSelectReturnedBrokenRow(t *testing.T) {
	mock := initDBMock(t)
	defer acc.DB.Close()

	testCase := &models.Account{
		ID:       1,
		Currency: "us dollar",
		Balance:  100,
	}

	stmt := `SELECT n_currency_id FROM accounts WHERE id = \$1`
	result := mock.NewRows([]string{"id"}).AddRow(1).RowError(0, fmt.Errorf("error"))
	mock.ExpectQuery(stmt).WithArgs(testCase.ID).WillReturnRows(result)

	id, err := GetAccountCurrencyID(acc.DB, testCase.ID)
	assert.Equal(t, 0, id)
	assert.NotNil(t, err)
}

func TestExistsSucceed(t *testing.T) {
	mock := initDBMock(t)
	defer acc.DB.Close()

	testCase := &models.Account{
		ID:       1,
		Currency: "us dollar",
		Balance:  100,
	}

	stmt := `SELECT COUNT\(id\) FROM accounts WHERE id = \$1`
	result := mock.NewRows([]string{"count"}).AddRow(1)
	mock.ExpectQuery(stmt).WithArgs(testCase.ID).WillReturnRows(result)

	res := Exists(acc.DB, testCase.ID)
	assert.Equal(t, true, res)
}

func TestExistsFailedOnSelectReturnedBrokenRow(t *testing.T) {
	mock := initDBMock(t)
	defer acc.DB.Close()

	testCase := &models.Account{
		ID:       1,
		Currency: "us dollar",
		Balance:  100,
	}

	stmt := `SELECT COUNT\(id\) FROM accounts WHERE id = \$1`
	result := mock.NewRows([]string{"count"}).AddRow(1).RowError(0, fmt.Errorf("error"))
	mock.ExpectQuery(stmt).WithArgs(testCase.ID).WillReturnRows(result)

	res := Exists(acc.DB, testCase.ID)
	assert.Equal(t, false, res)
}

func TestGetBalanceSucceed(t *testing.T) {
	mock := initDBMock(t)
	defer acc.DB.Close()

	testCase := &models.Account{
		ID:       1,
		Currency: "us dollar",
		Balance:  100,
	}

	stmt := `SELECT n_balance FROM accounts WHERE id = \$1`
	result := mock.NewRows([]string{"balance"}).AddRow(testCase.Balance)
	mock.ExpectQuery(stmt).WithArgs(testCase.ID).WillReturnRows(result)

	balance, err := GetBalance(acc.DB, testCase.ID)
	assert.Equal(t, testCase.Balance, balance)
	assert.Equal(t, nil, err)
}

func TestGetBalanceOnSelectReturnedBrokenRow(t *testing.T) {
	mock := initDBMock(t)
	defer acc.DB.Close()

	testCase := &models.Account{
		ID:       1,
		Currency: "us dollar",
		Balance:  100,
	}

	stmt := `SELECT COUNT\(id\) FROM accounts WHERE id = \$1`
	result := mock.NewRows([]string{"count"}).AddRow(1).RowError(0, fmt.Errorf("error"))
	mock.ExpectQuery(stmt).WithArgs(testCase.ID).WillReturnRows(result)

	balance, err := GetBalance(acc.DB, testCase.ID)
	assert.Equal(t, float64(0), balance)
	assert.NotNil(t, err)
}

func TestValidateAccountStartBalanceSucceed(t *testing.T) {
	testCase := &models.Account{
		ID:       1,
		Currency: "us dollar",
		Balance:  100,
	}

	code, err := validateAccountStartBalance(testCase)
	assert.Equal(t, 200, code)
	assert.Equal(t, nil, err)
}

func TestValidateAccountStartBalanceFailedOnCurrencyIsEmpty(t *testing.T) {
	testCase := &models.Account{
		ID:       1,
		Currency: "",
		Balance:  100,
	}

	code, err := validateAccountStartBalance(testCase)
	assert.Equal(t, 422, code)
	assert.Equal(t, fmt.Errorf("Account currency is empty (account=%d)", testCase.ID), err)
}

func TestValidateAccountStartBalanceFailedAccStartBalanceBelowZero(t *testing.T) {
	testCase := &models.Account{
		ID:       1,
		Currency: "us dollar",
		Balance:  -1,
	}

	code, err := validateAccountStartBalance(testCase)
	assert.Equal(t, 422, code)
	assert.Equal(t, fmt.Errorf("Account start balance is invalid (account=%d, balance=%.2f)", testCase.ID, testCase.Balance), err)
}

func TestValidateAccountStartBalanceFailedAccStartBalanceExceedsMax(t *testing.T) {
	testCase := &models.Account{
		ID:       1,
		Currency: "us dollar",
		Balance:  constants.MaxBalanceValue + 1,
	}

	code, err := validateAccountStartBalance(testCase)
	assert.Equal(t, 422, code)
	assert.Equal(t, fmt.Errorf("Account start balance is invalid (account=%d, balance=%.2f)", testCase.ID, testCase.Balance), err)
}
