// Package account includes types and methods to handle /accounts and /account/* requests.
package account

import (
	"database/sql"
	"fmt"

	log "github.com/go-pkgz/lgr"
	"gitlab.com/rtdevops/wallet-core/gen/models"
	"gitlab.com/rtdevops/wallet-core/internal/constants"
)

// createAccountHelper function makes all processing for /account/create requests.
// Validates user input and inserts data into Accounts table.
func createAccountHelper(db *sql.DB, params *models.Account) (int, error) {
	setPaymentDefaultCurrency(params)
	code, err := validateAccountStartBalance(params)
	if err != nil {
		return code, err
	}
	currencyID, err := GetCurrencyIDbyName(db, params.Currency)
	if err != nil {
		return 422, err
	}

	accountID, balance, errCtx := sql.NullInt64{}, float64(0), ""
	stmt := `INSERT INTO accounts (n_currency_id, n_balance) VALUES ($1, $2) RETURNING id, n_balance`
	row := db.QueryRow(stmt, currencyID, params.Balance)
	switch err = row.Scan(&accountID, &balance); err {
	case sql.ErrNoRows:
		errCtx = "Failed to create account"
	case nil:
		params.ID, params.Balance = accountID.Int64, balance
		log.Printf("DEBUG Account created (id=%d)", params.ID)
	default:
		errCtx = "Unexpected error. Failed to create account"
	}

	if err != nil {
		log.Printf("ERROR %s %s", err, printPaymentAttrs(params))
		return 500, fmt.Errorf("%s %s", errCtx, printPaymentAttrs(params))
	}
	return 200, nil
}

// getAccountsHelper function makes all processing for /accounts requests.
// Executes SELECT statement, validates and returns output.
func getAccountsHelper(db *sql.DB) ([]*models.Account, int, error) {
	var accounts []*models.Account

	stmt := `SELECT a.id, a.n_balance, c.vc_name
					 FROM accounts   a,
								currencies c
					 WHERE a.n_currency_id = c.n_currency_id`

	rows, err := db.Query(stmt)
	if err != nil {
		log.Printf("ERROR %s", err)
		return nil, 500, fmt.Errorf("Failed to initiate DB query")
	}
	defer rows.Close()

	for rows.Next() {
		var (
			accountID    sql.NullInt64
			balance      sql.NullFloat64
			currencyName sql.NullString
		)

		err = rows.Scan(&accountID, &balance, &currencyName)
		if err != nil {
			log.Printf("ERROR %s", err)
			return nil, 500, fmt.Errorf("Failed to parse returned rowset")
		}
		accounts = append(accounts, &models.Account{
			ID:       accountID.Int64,
			Balance:  balance.Float64,
			Currency: currencyName.String,
		})
	}
	err = rows.Err()
	if err != nil {
		log.Printf("ERROR %s", err)
		return nil, 500, fmt.Errorf("Failed to parse row")
	}
	return accounts, 200, nil
}

// GetCurrencyIDbyName function returns currency ID by currency name from the database
func GetCurrencyIDbyName(db *sql.DB, name string) (int, error) {
	stmt, currencyID := `SELECT n_currency_id FROM currencies WHERE trim(lower(vc_name)) = trim(lower($1))`, 0
	err := db.QueryRow(stmt, name).Scan(&currencyID)
	if currencyID == 0 {
		log.Printf("WARN %s (currency=%s)", err, name)
		return currencyID, fmt.Errorf("Currency name not found (currency=%s)", name)
	}
	return currencyID, nil
}

// GetAccountCurrencyID function returns account currency ID by account name from the database
func GetAccountCurrencyID(db *sql.DB, account int64) (int, error) {
	stmt, currencyID := `SELECT n_currency_id FROM accounts WHERE id = $1`, 0
	err := db.QueryRow(stmt, account).Scan(&currencyID)
	if err != nil {
		log.Printf("ERROR %s (account=%d)", err, account)
		return currencyID, fmt.Errorf("Unexpected error. Account currency not found (account=%d)", account)
	}
	return currencyID, nil
}

// Exists function checks if account exists or not in the database
func Exists(db *sql.DB, account int64) bool {
	stmt, count := `SELECT COUNT(id) FROM accounts WHERE id = $1`, 0
	err := db.QueryRow(stmt, account).Scan(&count)
	if count != 1 || err != nil {
		log.Printf("WARN Account not found, %s (account=%d)", err, account)
		return false
	}
	return true
}

// GetBalance function returns account balance by account ID from the database
func GetBalance(db *sql.DB, account int64) (float64, error) {
	stmt, balance := `SELECT n_balance FROM accounts WHERE id = $1`, float64(0)
	err := db.QueryRow(stmt, account).Scan(&balance)
	if err != nil {
		log.Printf("ERROR %s (account=%d)", err, account)
		return balance, fmt.Errorf("Unexpected error. Failed to get account balance (account=%d)", account)
	}
	return balance, nil
}

// validateAccountStartBalance function validates that passed start balance doesn't violate service's constraints.
func validateAccountStartBalance(params *models.Account) (int, error) {
	errCtx := ""
	if params.Currency == "" {
		errCtx = fmt.Sprintf("Account currency is empty (account=%d)", params.ID)
	}
	if params.Balance < float64(0) || params.Balance > constants.MaxBalanceValue {
		errCtx = fmt.Sprintf("Account start balance is invalid (account=%d, balance=%.2f)", params.ID, params.Balance)
	}
	if errCtx != "" {
		return 422, fmt.Errorf(errCtx)
	}
	return 200, nil
}

func setPaymentDefaultCurrency(params *models.Account) {
	if params.Currency == "" {
		params.Currency = constants.DefaultCurrency
	}
}

// printPaymentAttrs function prints account's details for convenient logging purpose.
func printPaymentAttrs(params *models.Account) string {
	res := "("
	if params.ID != 0 {
		res = fmt.Sprintf("id=%d, ", params.ID)
	}

	res = fmt.Sprintf("%sbalance=%.2f, ", res, params.Balance)

	if params.Currency != "" {
		res = fmt.Sprintf("%scurrency=%s", res, params.Currency)
	}
	return fmt.Sprintf("%s)", res)
}
