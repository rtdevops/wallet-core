package main

import (
	"database/sql"
	"testing"
	"time"

	sqlmock "github.com/DATA-DOG/go-sqlmock"
	"github.com/spf13/afero"
	"github.com/stretchr/testify/assert"
)

func TestParseConfigSucceed(t *testing.T) {
	appFS = afero.NewMemMapFs()

	exampleCfg := `service:
  host: 0.0.0.0
  port: 8080
database:
  host: 127.0.0.1
  port: 5432
  ssl: false # true | false
  name: walletcore
  user: walletcore
  password: password`

	afero.WriteFile(appFS, "config.test.yml", []byte(exampleCfg), 0644)

	conf := parseConfig("config.test.yml")
	assert.Equal(t, "0.0.0.0", conf.Service.Host)
	assert.Equal(t, 8080, conf.Service.Port)
	assert.Equal(t, "127.0.0.1", conf.Database.Host)
	assert.Equal(t, 5432, conf.Database.Port)
	assert.Equal(t, false, conf.Database.SSL)
	assert.Equal(t, "walletcore", conf.Database.Name)
	assert.Equal(t, "walletcore", conf.Database.User)
	assert.Equal(t, "password", conf.Database.Password)
}

func TestWaitForDBSucceed(t *testing.T) {
	db, _, err := sqlmock.New()
	if err != nil {
		t.Fatalf("an error '%s' was not expected when opening a stub database connection", err)
	}
	res := waitForDB(db, time.Duration(3))
	assert.Equal(t, true, res)
}

func TestWaitForDBFailedPing(t *testing.T) {
	db, _ := sql.Open("postgres", "")
	res := waitForDB(db, time.Duration(2))
	assert.Equal(t, false, res)
}
