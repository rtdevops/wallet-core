# wallet-core

The service provides a basic RESTful money wallet implementation. Project is based on [go-swagger](https://github.com/go-swagger/go-swagger) (OpenAPI v2.0). Business logic is located under `internal/` folder.

This software is provided "as is" without warranty of any kind.

## Known limitations
* By default, 3 currencies are available: `US Dollar`, `Euro`, `British Pound`
* Only transfers between accounts in the same currency are supported (no exchanges)
* Account's balance can't go below zero
* Minimum payment amount is `0.01`, maximum is `10000000000000.99`
* Maximum account's balance is limited to `10000000000000.99`
* Stored number of digits after the decimal point is `2` (both for transfers and accounts' balances)
* Default currency for all operations is `US Dollar` if not passed explicitly

## Quickstart

Use `docker-compose`. It will build and run go binary for you as well as a tiny Postgres installation. 

```
$ rm -rf docker/data/db/.keep && docker-compose -f docker/docker-compose.yml up -d
```

By default, the service will listen on `http://127.0.0.1:8080`. Cold start may take up to 30 seconds due to Postges initialization time.
Recommended versions: Docker 19.03.2, docker-compose 1.24.1. Tested under macOS 10.14.6 (Mojave)

## API

| Endpoint             | Method | Auth? | Example Payload                                                    | Description                                             |
| -------------------- | ------ | ----- | ------------------------------------------------------------------ | ------------------------------------------------------- |
| `/v1/account/create` | POST   | No    | {"currency":"euro", "balance": 100}                                | Create new account with set starting balance (optional) |
| `/v1/accounts`       | GET    | No    | No                                                                 | Get all accounts                                        |
| `/v1/payment/make`   | POST   | No    | {"fromAccount": 28, "toAccount":29, "amount":1, "currency":"euro"} | Make a payment                                          |
| `/v1/payments`       | GET    | No    | No                                                                 | Get all payments                                        |

Default `docker-compose.yml` includes additional container which serves more detailed auto-generated API documentation. It's available on `http://127.0.0.1:8081/`. You also may start that container separately:
```
$ docker-compose -f docker/docker-compose.yml up wallet-core-spec
```

### API Examples

**Create account with 0 balance**

```
$ curl -X POST --compressed -H 'Content-Type: application/json' -H 'Accept-Encoding: gzip' -d '{"currency":"euro"}' 'http://127.0.0.1:8080/v1/account/create'
```

Expected output: 

```
{"id":1,"balance":0,"currency":"euro"}
```

**Create account with non-zero balance**

```
$ curl -X POST --compressed -H 'Content-Type: application/json' -H 'Accept-Encoding: gzip' -d '{"currency":"euro", "balance":500}' 'http://127.0.0.1:8080/v1/account/create'
```

Expected output: 

```
{"id":2,"balance":500,"currency":"euro"}
```

**Get all accounts**

```
$ curl -X GET --compressed -H 'Content-Type: application/json' -H 'Accept-Encoding: gzip' 'http://127.0.0.1:8080/v1/accounts'
```

Expected output:

```
[{"id":1,"balance":0,"currency":"Euro"},{"id":2,"balance":500,"currency":"Euro"}]
```

**Make Payment**

```
$ curl -X POST --compressed -H 'Content-Type: application/json' -H 'Accept-Encoding: gzip' -d '{"fromAccount": 2, "toAccount":1, "amount":100.99, "currency":"euro"}' 'http://127.0.0.1:8080/v1/payment/make'
```

Expected output:

```
{"id":1,"amount":100.99,"currency":"euro","fromAccount":2,"toAccount":1}
```

**Get all payments**

```
$ curl -X GET --compressed -H 'Content-Type: application/json' -H 'Accept-Encoding: gzip' 'http://127.0.0.1:8080/v1/payments'
```

Expected output:

```
[{"id":1,"amount":100.99,"currency":"Euro","fromAccount":2,"toAccount":1}]
```

## Advanced configuration

The service is supplied with reasonable defaults and ready to be started locally by docker-compose. Though it's possible to adjust some parameters.

*config.yml*

```
service:
  host: 0.0.0.0
  port: 8080
database:
  host: 127.0.0.1
  port: 5432
  ssl: false # true | false
  name: walletcore
  user: walletcore
  password: password
```

In config file most options are self-explained. It stores database configuration and host/port the service needs to bind to. In `database` section, `ssl` option may be enabled to establish communication between the service and Postgres database over an encrypted channel (self-signed certificates are supplied with the service). In addition to this option, the corresponding line in `docker-compose.yml` must be uncommented. These changes require Postgres restart and service's image rebuild.

*CLI flags*

```
$ wallet-core -h
Usage:
  main [OPTIONS]

Application Options:
  -c, --config= Path to the configuration file (default: config.yml) [$SHA2GEN_CONFIG_PATH]
  -d, --debug   Debug mode [$SHA2GEN_DEBUG_MODE]

Help Options:
  -h, --help    Show this help message
```

Debug mode enables verbose logging. 

## How to re-generate Swagger server code

```
$ swagger generate server -t $(pwd)/gen -f $(pwd)/swagger/swagger.yml  --template=stratoscale --exclude-main -A wallet-core
```

## Tests

All the files under `gen/` are generated by `go-swagger` and don't need tests. Business logic (handlers) is mostly covered.
```
$ go test -cover ./...
ok      gitlab.com/rtdevops/wallet-core (cached)        coverage: 29.6% of statements
?       gitlab.com/rtdevops/wallet-core/gen/models      [no test files]
?       gitlab.com/rtdevops/wallet-core/gen/restapi     [no test files]
?       gitlab.com/rtdevops/wallet-core/gen/restapi/operations  [no test files]
?       gitlab.com/rtdevops/wallet-core/gen/restapi/operations/account  [no test files]
?       gitlab.com/rtdevops/wallet-core/gen/restapi/operations/payment  [no test files]
ok      gitlab.com/rtdevops/wallet-core/internal/account        (cached)        coverage: 81.7% of statements
?       gitlab.com/rtdevops/wallet-core/internal/constants      [no test files]
ok      gitlab.com/rtdevops/wallet-core/internal/payment        0.036s  coverage: 86.0% of statements
```

## Contribution

PRs are very welcomed.
Please, use `golint` and `gofmt` before submitting a PR and don't forget about tests.

## TODO

- Handle gzipped requests
- Consider cache layer
- Simplify logging
- Write more tests

## License

MIT
