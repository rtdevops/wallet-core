CREATE TABLE IF NOT EXISTS currencies (
  id            SERIAL PRIMARY KEY,
  n_currency_id INT UNIQUE NOT NULL,
  vc_name       VARCHAR(32) NOT NULL,
  vc_sign       VARCHAR(6) NOT NULL
);

CREATE TABLE IF NOT EXISTS accounts (
  id            SERIAL PRIMARY KEY,
  n_balance     NUMERIC(19,2) NOT NULL DEFAULT 0.00,
  n_currency_id INT NOT NULL DEFAULT 1
);

CREATE TABLE IF NOT EXISTS payments (
  id                SERIAL PRIMARY KEY,
  n_from_account_id INT NOT NULL,
  n_to_account_id   INT NOT NULL,
  n_amount          NUMERIC(19,2) NOT NULL,
  n_currency_id     INT NOT NULL DEFAULT 1
);

ALTER TABLE accounts ADD FOREIGN KEY (n_currency_id) REFERENCES currencies (n_currency_id);
ALTER TABLE payments ADD FOREIGN KEY (n_currency_id) REFERENCES currencies (n_currency_id);
ALTER TABLE payments ADD FOREIGN KEY (n_from_account_id) REFERENCES accounts (id);
ALTER TABLE payments ADD FOREIGN KEY (n_to_account_id) REFERENCES accounts (id);

INSERT INTO currencies (n_currency_id, vc_name, vc_sign)
VALUES
  (1, 'US Dollar', '$'),
  (2, 'Euro', '€'),
  (3, 'British Pound', '£')
ON CONFLICT DO NOTHING;